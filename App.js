import React, { Component } from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    Image,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import { Provider } from 'react-redux';
import { StackNavigator, NavigationActions } from 'react-navigation';

import configureStore from './app/configureStore';

import Login from './app/screens/Login';
import Groups from './app/screens/Groups';
import Group from './app/screens/Group';
import Signup from './app/screens/Signup';

import { GROUPS_SHOW_MODAL } from './app/constaint/groups';
import { LOGOUT } from './app/constaint/user';

const AppRouterConfig = {
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Авторизация'
        }
    },
    Groups: {
        screen: Groups,
        navigationOptions: {
            title: 'Группы',
            headerLeft: null

        }
    },
    Group: {
        screen: Group,
        navigationOptions: ({ navigation }) => ({
            title: navigation.state.params.name
        })
    },
    Signup: {
        screen: Signup,
        navigationOptions: {
            title: 'Регистрация',
            headerTruncatedBackTitle: 'Назад'
        }
    }
};

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fetchedUser: false,
            user: null
        };
    }
    componentDidMount() {
        AsyncStorage.getItem('user').then(user => {
            this.setState({
                fetchedUser: true,
                user
            });
        });
    }
    renderHeaderRight(options) {
        const {
            navigation,
            dispatch,
            user
        } = options;
        const { routeName } = navigation.state;

        return (
            <View style={styles.headerRight}>
                {
                    routeName === 'Groups' ?
                        <TouchableHighlight
                            style={styles.headerRightTouch}
                            activeOpacity={.7}
                            underlayColor="transparent"
                            onPress={() => {
                                dispatch({
                                    type: GROUPS_SHOW_MODAL
                                })
                            }}
                        >
                            <Image
                                style={styles.headerIcon}
                                source={require('./app/img/plus.png')}/>
                        </TouchableHighlight>
                    : null
                }
                {
                    user && user.isAuthorized ?
                        <TouchableHighlight
                            style={styles.headerRightTouch}
                            activeOpacity={.7}
                            underlayColor="transparent"
                            onPress={() => {
                                dispatch({
                                    type: LOGOUT
                                });

                                const loginAction = NavigationActions.reset({
                                    index: 0,
                                    actions: [
                                        NavigationActions.navigate({ routeName: 'Login' })
                                    ]
                                });

                                navigation.dispatch(loginAction);
                            }}
                        >
                            <Image
                                style={styles.headerIcon}
                                source={require('./app/img/sign-out.png')}/>
                        </TouchableHighlight>
                    : null
                }
            </View>
        );
    }
    render() {
        if (this.state.fetchedUser) {
            const store = configureStore(this.state.user);
            const { dispatch } = store;
            const AppNavigator = StackNavigator(AppRouterConfig, {
                initialRouteName: this.state.user ? 'Groups' : null,
                navigationOptions: ({ navigation }) => {
                    const { user } = store.getState();

                    return {
                        headerRight: this.renderHeaderRight({ navigation, dispatch, user })
                    };
                }
            });

            return (
                <Provider store={store}>
                    <AppNavigator />
                </Provider>
            );
        } else {
            return (
                <ActivityIndicator />
            );
        }

    }
}

const styles = StyleSheet.create({
    headerRight: {
        flex: 1,
        flexDirection: 'row',
    },
    headerRightTouch: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    headerIcon: {
        width: 18,
        height: 18
    }
});

export default App;
