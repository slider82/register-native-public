export const LESSON_SAVE_SEND = 'LESSON_SAVE_SEND';
export const LESSON_SAVE_SUCCESS = 'LESSON_SAVE_SUCCESS';

export const LESSON_GET_SEND = 'LESSON_GET_SEND';
export const LESSON_GET_SUCCESS = 'LESSON_GET_SUCCESS';

export const LESSON_DEFAULT_PARAMS = [
    '_id',
    'credit',
    'date',
    'evaluation',
    'groupId',
    'presence',
    'student',
    'testPeriodNumber'
];