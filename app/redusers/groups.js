import {
    GROUPS_GET_FETCHING,
    GROUPS_GET_SUCCESS,
    // GROUP_CREATE_REQUEST,
    GROUP_CREATE_SUCCESS,
    GROUPS_SHOW_MODAL,
    GROUPS_HIDE_MODAL
} from '../constaint/groups';
import {
    LOGOUT
} from '../constaint/user';
const initialState = {
    fetched: false,
    fetching: false,
    showModal: false,
    items: []
};

export default function groups(state = initialState, action) {
    switch (action.type) {
        case GROUPS_SHOW_MODAL:
            return {
                ...state,
                showModal: true
            };
        case GROUPS_HIDE_MODAL:
            return {
                ...state,
                showModal: false
            };
        case GROUPS_GET_FETCHING:
            return {
                ...state,
                fetching: true
            };
        case GROUPS_GET_SUCCESS:
            return {
                ...state,
                fetching: false,
                fetched: true,
                items: action.groups
            };
        case GROUP_CREATE_SUCCESS:
            return {
                ...state,
                items: state.items.concat([action.group])
            };
        case LOGOUT:
            return {
                ...initialState
            };
        default:
            return state;
    }
}