import _ from 'lodash';
import {
    LESSON_SAVE_SEND,
    LESSON_SAVE_SUCCESS,
    LESSON_GET_SEND,
    LESSON_GET_SUCCESS
} from '../constaint/lessons';
import {
    LOGOUT
} from '../constaint/user';

const initialState = {};

export default function lessons(state = initialState, action) {
    switch(action.type) {
        case LESSON_SAVE_SEND:
            return state;
        case LESSON_SAVE_SUCCESS:
            return _.merge({}, state, {
                [action.lesson.groupId]: {
                    [action.lesson.student]: {
                        [action.lesson.date]: action.lesson
                    }
                }
            });
        case LESSON_GET_SEND:
            return state;
        case LESSON_GET_SUCCESS:
            return _.merge({}, state, {
                [action.groupId]: {},
                ...action.lessons
            });
        case LOGOUT:
            return {};
        default:
            return state;
    }
}