'use strict';

import _ from 'lodash';
import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_FAIL,
    LOGOUT
} from '../constaint/user';

const initialState = {
    fetching: false,
    name: '',
    email: '',
    token: '',
    isAuthorized: false,
    error: null
};

export default function user(state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
        case SIGNUP_REQUEST:
            return {
                ...state,
                fetching: true,
                error: null
            };
        case LOGIN_SUCCESS:
        case SIGNUP_SUCCESS:
            return {
                ...state,
                fetching: false,
                ...action.user,
                token: action.token,
                isAuthorized: true,
                error: null
            };
        case LOGIN_FAIL:
            return {
                fetching: false,
                error: _.extend({}, action.error, {
                    type: 'login'
                })
            };
        case SIGNUP_FAIL:
            return {
                fetching: false,
                error: _.extend({}, action.error, {
                    type: 'signup'
                })
            };
        case LOGOUT:
            return {
                ...initialState
            };
        default:
            return state
    }
}