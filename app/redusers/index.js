'use strict';

import { combineReducers } from 'redux';

import user from './user';
import groups from './groups';
import lessons from './lessons';


export default combineReducers({
    user,
    groups,
    lessons
});