'use strict';

import React, { Component } from 'react';
import {
    ActivityIndicator,
    Animated,
    Modal,
    PanResponder,
    Platform,
    Text,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { LESSON_DEFAULT_PARAMS } from '../constaint/lessons';
import {
    saveLesson,
    getLesson
} from '../actions/LessonActions';
import ModalLesson from '../components/ModalLesson';

class Group extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDates: 3,
            fetchedLessons: false,
            lesson: {},
            date: '',
            student: {},
            modalVisible: false,
            opacity: new Animated.Value(1)
        };

        this.onPanResponderMove = this.onPanResponderMove.bind(this);
        this.onPanResponderRelease = this.onPanResponderRelease.bind(this);
        this.onPressCloseModal = this.onPressCloseModal.bind(this);
        this.onPressSubmitModal = this.onPressSubmitModal.bind(this);
    }

    componentWillMount() {
        const { id } = this.props.navigation.state.params;

        // fetch lessons
        if (!this.props.lessons[id]) {
            this.props.getLesson(id, this.props.token);
        } else {
            this.setState({
                fetchedLessons: true
            });
        }

        this.group = this.props.groups.filter(item => item._id === id)[0];

        this.dates = this.group.edu.dates;
        this.students = this.group.students.map(student => {
            return _.merge({}, student, {
                shortName: student.name
                    .split(' ')
                    .map((item, index) => index === 0 ? item : item[0].toUpperCase() + '.')
                    .join(' ')
            });
        });
        this.currentDate = this.group.edu.currentDate;

        this.offsetX = 0;

        this.screen = Math.floor(this.dates.indexOf(this.currentDate) / this.state.showDates);
        this.screenLength = Math.floor(this.dates.length / this.state.showDates);

        this.setState({
            dates: this.getDatesByScreen(this.screen)
        });

        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: this.onMoveShouldSetPanResponder,
            onPanResponderMove: this.onPanResponderMove,
            onPanResponderRelease: this.onPanResponderRelease,
            onPanResponderTerminate: this.onPanResponderRelease,
        });

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.lessons[this.group._id]) {
            this.setState({
                fetchedLessons: true
            });
        }
    }

    onMoveShouldSetPanResponder(e, gestureState) {
        return Math.abs(gestureState.dx) > 10;
    }

    onPanResponderMove(e, gestureState) {
        if (gestureState.dx !== 0) {
            this.offsetX = this.offsetX + gestureState.dx;
        }
    }

    onPanResponderRelease(e, gestureState) {
        if (gestureState.dx !== 0) {
            this.offsetX = this.offsetX + gestureState.dx;

            if (Math.abs(this.offsetX) > 50) {

                if (this.offsetX > 0) {
                    this.offsetX = 0;
                }

                if (this.offsetX < 0) {
                    this.screen = this.screen + 1;
                } else {
                    this.screen = this.screen - 1;
                }

                if (this.screen >= 0 && this.screen <= this.screenLength) {
                    const duration = 300;

                    Animated.sequence([ // One after the other
                        Animated.timing(this.state.opacity, {
                            toValue: 0,
                            duration: duration
                        }),
                        Animated.timing(this.state.opacity, {
                            toValue: 1
                        }),
                    ]).start();

                    setTimeout(() => {
                        this.setState({
                            dates: this.getDatesByScreen(this.screen)
                        });
                    }, duration);
                } else {
                    if (this.screen < 0) {
                        this.screen = 0;
                    }

                    if (this.screen > this.screenLength) {
                        this.screen = this.screenLength;
                    }
                }

                this.offsetX = 0;
            }
        }
    }

    onPressStudentDate(options) {
        const {student, date, lesson } = options;

        this.setState({
            modalVisible: true,
            lesson,
            student,
            date
        });
    }

    onPressCloseModal() {
        this.setState({
            modalVisible: false,
            lesson: {},
            student: {},
            date: ''
        });
    }

    onPressSubmitModal(props) {
        const lessonProps = _.merge({}, _.pick(this.state.lesson, LESSON_DEFAULT_PARAMS), {
            student: this.state.student.id,
            groupId: this.group._id,
            date: this.state.date,
            testPeriodNumber: null,
            ...props
        });

        this.props.saveLesson(lessonProps, this.props.token);

        this.setState({
            modalVisible: false,
            lesson: {},
            student: {},
            date: ''
        });
    }

    getDatesByScreen(screen) {
        if (screen < 0) return null;

        const startDate = screen * this.state.showDates;
        const endDate = startDate + this.state.showDates;

        return this.dates.slice(startDate, endDate);
    }

    renderHead() {
        const { dates } = this.state;

        return (
            <View style={[styles.row, styles.list]}>
                <View style={styles.colLeft}>
                    <View style={styles.col}>
                        <Text style={[styles.text, styles.textHead]}>Студенты</Text>
                    </View>
                </View>
                <View style={styles.colRight}>
                    <View style={styles.row}>
                        {
                            dates.map((date, idx) => {
                                const stylesCurrentDate = date === this.currentDate ? styles.colCurrentDate : {};

                                return (<View style={[styles.col, stylesCurrentDate]} key={idx}>
                                    <Animated.View
                                        style={{
                                            opacity: this.state.opacity,
                                            flex: 1
                                        }}>
                                        <Text style={[
                                            styles.text,
                                            styles.textCenter,
                                            styles.textHead]}
                                        >{date}</Text>
                                    </Animated.View>
                                </View>)
                            })
                        }
                    </View>
                </View>
            </View>
        );
    }

    renderLeft(students) {
        return <View
            style={styles.colLeft}
        >{ students.map((item, index) => {
            return (
                <View key={index} style={styles.col}>
                    <Text style={[styles.text]}>{item.shortName}</Text>
                </View>
            );
        })
        }</View>;
    }

    renderRight(students, dates) {
        const { _id } = this.group;
        const groupLessons = this.props.lessons[_id] || {};

        return students.map((student, index) => {
            const lessons = groupLessons[student.id];

            return <View key={index} style={styles.row}>
                { dates.map((date, idx) => {
                        const stylesCurrentDate = date === this.currentDate ? styles.colCurrentDate : {};
                        const lesson = lessons && lessons[date] || {};
                        let presence, evaluation, credit;

                        if (lesson._id) {
                            if (lesson.credit) {
                                credit = 'З.';
                            } else {
                                if (lesson.evaluation) {
                                    evaluation = lesson.evaluation;
                                } else {
                                    presence = lesson.presence ? 'Б.' : 'H/Б';
                                }
                            }
                        }

                        return <View
                            style={[styles.col, stylesCurrentDate, { padding: 0 }]}
                            key={idx}
                        >
                            <Animated.View
                                style={{
                                    opacity: this.state.opacity,
                                    flex: 1
                                }}>
                                <TouchableHighlight
                                    style={[styles.touchableText]}
                                    activeOpacity={.7}
                                    underlayColor="transparent"
                                    onPress={() => this.onPressStudentDate({ student, date, lesson })}
                                >
                                    <Text style={[styles.text, styles.textCenter]}>
                                        {presence}
                                        {evaluation}
                                        {credit}
                                    </Text>
                                </TouchableHighlight>
                            </Animated.View>
                        </View>
                    })
                }
            </View>
        });
    }

    render() {
        if (!this.state.fetchedLessons) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator
                        size="large"
                    />
                </View>
            );
        }

        const group = this.group;
        let dates = this.state.dates;
        let students = group.students.map(student => {
            return _.merge({}, student, {
                shortName: student.name
                    .split(' ')
                    .map((item, index) => index === 0 ? item : item[0].toUpperCase() + '.')
                    .join(' ')
            });
        });

        const renderHead = this.renderHead();
        const renderLeft = this.renderLeft(students);
        const renderRight = this.renderRight(students, dates);

        return (
            <View style={styles.container}>
                { renderHead }
                <ScrollView>
                    <View
                        style={[styles.row, styles.list, styles.listBody]}
                        {...this._panResponder.panHandlers}
                    >
                        { renderLeft }
                        <View style={[styles.colRight]}>
                            { renderRight }
                        </View>
                    </View>
                </ScrollView>

                <ModalLesson
                    modalVisible={this.state.modalVisible}
                    lesson={this.state.lesson}
                    date={this.state.date}
                    student={this.state.student}
                    onPressCloseModal={this.onPressCloseModal}
                    onPressSubmitModal={this.onPressSubmitModal}
                />
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 17
    },
    list: {
        paddingTop: 1,
        paddingLeft: 1
    },
    listBody: {
        marginTop: -1
    },
    row: {
        flexDirection: 'row',
    },

    colLeft: {
        flex: 4
    },
    colRight: {
        flex: 5,
        overflow: 'hidden',
        marginTop: -1,
        paddingTop: 1
    },

    col: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#CCC',
        backgroundColor: '#FFF',
        padding: 7,
        marginLeft: -1,
        marginTop: -1,
        height: 36,
    },
    colCurrentDate: {
        backgroundColor: '#eee'
    },
    text: {
        fontSize: 16,
    },
    textCenter: {
        textAlign: 'center'
    },
    textHead: {
        fontWeight: 'bold'
    },
    touchableText: {
        flex: 1,
        justifyContent: 'center'
    },

});

function mapStateToProps(state) {
    return {
        token: state.user.token,
        groups: state.groups.items,
        lessons: state.lessons,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        saveLesson: bindActionCreators(saveLesson, dispatch),
        getLesson: bindActionCreators(getLesson, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Group);