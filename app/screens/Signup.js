'use strict';

import React, { Component } from 'react';
import {
    ActivityIndicator,
    KeyboardAvoidingView,
    Text,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { signup } from '../actions/SignupActions';

class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            name: '',
            error: false,
            errorEmail: false,
            errorPassword: false,
            errorName: false
        };

        this.onFocus = this.onFocus.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { user } = nextProps;
        const { navigate } = this.props.navigation;

        if (user.error && user.error.type === 'signup') {
            let error = {
                error: true
            };

            switch (user.error.message) {
                case 'INCORRECT_EMAIL':
                    error.errorEmail = true;
                    break;
                case 'INCORRECT_PASSWORD':
                    error.errorPassword = true;
                    break;
                case 'EMPTY_FIELDS':
                    error.errorEmail = true;
                    error.errorPassword = true;
                    break;
            }

            this.setState(error);
        }

        if (user.isAuthorized) {
            navigate('Groups');
            this.reset(true);
        }
    }

    reset(force) {
        if (this.state.error || force) {
            this.setState({
                email: '',
                password: '',
                name: '',
                error: false,
                errorEmail: false,
                errorPassword: false,
                errorName: false
            });
        }
    }

    onFocus() {
        if (this.state.error) {
            this.setState({
                email: '',
                error: false,
                errorEmail: false,
            });
        }
    }

    onChangeEmail(value) {
        if (this.state.error) {
            this.reset();
        } else {
            this.setState({
                email: value.trim()
            });
        }
    }

    onChangePassword(value) {
        if (this.state.error) {
            this.reset();
        } else {
            this.setState({
                password: value.trim()
            });
        }
    }

    onChangeName(value) {
        this.setState({
            name: value
        });
    }

    onSubmit() {
        let { email, password, name } = this.state;
        name = name.trim();

        if (email && password && name) {
            this.props.signup({
                email,
                password,
                name
            });
        }
    }
    render() {
        const errorInputEmail = this.state.errorEmail ? styles.inputError : {};
        const { user } = this.props;
        const isShowPreloader = user.fetching;
        const buttonDisabled = !this.state.email || !this.state.password || !this.state.name || user.fetching;
        const buttonDisabledStyle = buttonDisabled ? styles.buttonDisabled : {};

        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                <View style={styles.row}>
                    <View style={styles.label}>
                        <Text>Email</Text>
                    </View>
                    <View style={styles.inputWrapper}>
                        <TextInput
                            autoCapitalize="none"
                            onFocus={this.onFocus}
                            onChangeText={this.onChangeEmail}
                            style={[styles.input, errorInputEmail]}
                            placeholder="Введите email"
                            value={this.state.email}
                        />
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.label}>
                        <Text>Пароль</Text>
                    </View>
                    <View style={styles.inputWrapper}>
                        <TextInput
                            autoCapitalize="none"
                            onFocus={this.onFocus}
                            onChangeText={this.onChangePassword}
                            style={styles.input}
                            placeholder="Введите пароль"
                            value={this.state.password}
                        />
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.label}>
                        <Text>Имя</Text>
                    </View>
                    <View style={styles.inputWrapper}>
                        <TextInput
                            autoCapitalize="none"
                            onFocus={this.onFocus}
                            onChangeText={this.onChangeName}
                            style={styles.input}
                            placeholder="Введите имя"
                            value={this.state.name}
                        />
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.label}/>
                    <View style={styles.buttonWrapper}>
                        <TouchableHighlight
                            onPress={this.onSubmit}
                            disabled={buttonDisabled}
                            style={[styles.button, buttonDisabledStyle]}
                        >
                            <Text style={styles.buttonText}>Регистрация</Text>
                        </TouchableHighlight>

                        {isShowPreloader ? <ActivityIndicator /> : null}
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 17,
        backgroundColor: '#FFF',
        // paddingTop: 100
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
    },
    inputWrapper: {
        flex: 3,
    },
    input: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 4,
        fontSize: 16,
        height: 30,
        backgroundColor: '#FFF'
    },
    inputError: {
        borderColor: '#a94442'
    },
    label: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    buttonWrapper: {
        flex: 3,
        flexDirection: 'row',
    },
    button: {
        height: 30,
        backgroundColor: '#337ab7',
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 4,
        marginRight: 10,
        padding: 10,
        justifyContent: 'center'
    },
    buttonDisabled: {
        opacity: .5
    },
    buttonSecond: {
        backgroundColor: '#FFF',
        borderColor: '#CCC'
    },
    buttonText: {
        color: '#FFF',
    }
});

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        signup: bindActionCreators(signup, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);