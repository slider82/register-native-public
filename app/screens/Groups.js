'use strict';

import React, { Component } from 'react';
import {
    ActivityIndicator,
    Image,
    ListView,
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getGroups, showModal } from '../actions/GroupActions';
import ModalCreateGroup from '../components/ModalCreateGroup';

class Groups extends Component {

    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            ds
        };

        this.onPressCreateGroup = this.onPressCreateGroup.bind(this);
    }
    componentDidMount() {
        if (!this.props.fetched) {
            const { token } = this.props;
            this.props.getGroups(token);
        }
    }

    onPressItem(group) {
        const { navigate } = this.props.navigation;

        navigate('Group', {
            id: group._id,
            name: group.name,
        });
    }

    onPressCreateGroup() {
        this.props.showModal();
    }

    render() {
        const { groups, navigation } = this.props;
        const { state, setParams } = navigation;
        const renderRow = (group) => <TouchableHighlight
                onPress={() => {
                    this.onPressItem(group);
                }}
                style={styles.listItem}
                underlayColor='#BBB'
            >
                <Text style={styles.listText}>{group.name}</Text>
            </TouchableHighlight>;
        const renderSeparator = () => <View style={styles.itemSeparator} />;

        return(
            <View style={styles.container}>
                {
                    this.props.fetched
                        ?
                        <View style={{flex: 1}}>
                            <ModalCreateGroup />
                            {
                                groups.length ?
                                    <ListView
                                        getRowCount={this.props.groups.length}
                                        dataSource={this.state.ds.cloneWithRows(groups)}
                                        renderRow={renderRow}
                                        renderSeparator={renderSeparator}
                                    />
                                    :
                                    <View style={styles.empty}>
                                        <Text style={styles.emptyText}>У вас нет групп, вы можете</Text>
                                        <TouchableOpacity
                                            style={styles.button}
                                            activeOpacity={.8}
                                            onPress={this.onPressCreateGroup}
                                        >
                                            <Text style={styles.buttonText}>Создать</Text>
                                        </TouchableOpacity>
                                    </View>
                            }
                        </View>
                        :
                        <View style={styles.preloader}>
                            <ActivityIndicator
                                size="large"
                            />
                        </View>
                }
            </View>
        );
    }
}

let styles = StyleSheet.create({
    headerRight: {
        flex: 1,
        flexDirection: 'row',
    },
    headerRightTouch: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    headerIcon: {
        width: 18,
        height: 18
    },

    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    empty: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 17,
    },
    emptyText: {
        fontSize: 16
    },
    preloader: {
        padding: 17
    },
    listItem: {
        height: 36,
        paddingLeft: 17,
        paddingTop: 7,
    },
    itemSeparator: {
        marginLeft: 17,
        height: 0.5,
        backgroundColor: '#CCC'
    },
    listText: {
        alignSelf: 'stretch',
        fontSize: 16
    },
    button: {
        marginTop: 10,
        height: 30,
        backgroundColor: '#337ab7',
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 4,
        marginRight: 10,
        padding: 10,
        justifyContent: 'center'
    },
    buttonText: {
        color: '#FFF',
        fontSize: 16
    }
});

function mapStateToProps(state) {
    return {
        token: state.user.token,
        groups: state.groups.items,
        fetching: state.groups.fetching,
        fetched: state.groups.fetched,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getGroups: bindActionCreators(getGroups, dispatch),
        showModal: bindActionCreators(showModal, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Groups);