import fetch from '../fetch';
import {
    LESSON_SAVE_SEND,
    LESSON_SAVE_SUCCESS,
    LESSON_GET_SEND,
    LESSON_GET_SUCCESS
} from '../constaint/lessons';

import { apiHost } from '../config'

export function saveLesson(lesson = {}, token) {
    return function (dispatch) {
          dispatch({
              type: LESSON_SAVE_SEND
          });

          fetch(`${apiHost}/api/lesson/save`, {
              method: 'POST',
              body: JSON.stringify(lesson)
          }, token).then(lesson => {
              dispatch({
                  type: LESSON_SAVE_SUCCESS,
                  lesson
              });
          });
    };
}

export function getLesson(groupId, token) {
    return function (dispatch) {

        dispatch({
            type: LESSON_GET_SEND
        });

        fetch(`${apiHost}/api/lesson/get`, {
            method: 'POST',
            body: JSON.stringify({
                groupId: groupId
            })
        }, token).then(lessons => {
            dispatch({
                type: LESSON_GET_SUCCESS,
                lessons,
                groupId
            });
        });
    }

}