'use strict';

import {
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_FAIL
} from '../constaint/user';
import fetch from '../fetch';
import { apiHost } from '../config';

export function signup(data) {
    return function (dispatch) {
        dispatch({
            type: SIGNUP_REQUEST
        });

        fetch(`${apiHost}/auth/signup`, {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(data => {
            if (data.error) {
                dispatch({
                    type: SIGNUP_FAIL,
                    error: data.error
                });
            } else {
                dispatch({
                    type: SIGNUP_SUCCESS,
                    ...data
                });
            }
        }).catch(error => {
            dispatch({
                type: SIGNUP_FAIL,
                error
            });
        });

    }
}