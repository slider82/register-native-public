'use strict';

import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT
} from '../constaint/user';
import fetch from '../fetch';
import { apiHost } from '../config';

export function login(data) {
    return function (dispatch) {

        dispatch({
            type: LOGIN_REQUEST
        });

        fetch(`${apiHost}/auth/login`, {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(data => {
            if (data.error) {
                dispatch({
                    type: LOGIN_FAIL,
                    error: data.error
                });
            } else {
                dispatch({
                    type: LOGIN_SUCCESS,
                    ...data
                });
            }
        }).catch(error => {
            dispatch({
                type: LOGIN_FAIL,
                error
            });
        });

    }
}

export function logout() {
    return function (dispatch) {
        dispatch({
            type: LOGOUT
        });
    };
}