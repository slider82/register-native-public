import fetch from '../fetch';
import {
    GROUPS_GET_FETCHING,
    GROUPS_GET_SUCCESS,
    GROUP_CREATE_REQUEST,
    GROUP_CREATE_SUCCESS,
    GROUPS_HIDE_MODAL,
    GROUPS_SHOW_MODAL
} from '../constaint/groups';

import { apiHost } from '../config'

export function getGroups(token) {
    return function (dispatch) {
        dispatch({
            type: GROUPS_GET_FETCHING
        });

        fetch(`${apiHost}/api/group/get`, null, token)
            .then(response => {
                const { groups } = response;

                dispatch({
                    type: GROUPS_GET_SUCCESS,
                    groups
                });
            });
    }
}

export function createGroup(data = {}, token) {
    return function (dispatch) {
        dispatch({
            type: GROUP_CREATE_REQUEST
        });
        fetch(`${apiHost}/api/group/create`, {
            method: 'POST',
            body: JSON.stringify(data)
        }, token).then(group => {
            dispatch({
                type: GROUP_CREATE_SUCCESS,
                group: group
            });
        }).catch(err => {
            console.log(err)
        });
    }
}

export function hideModal() {
    return function (dispatch) {
        dispatch({
            type: GROUPS_HIDE_MODAL
        });
    }
}

export function showModal() {
    return function (dispatch) {
        dispatch({
            type: GROUPS_SHOW_MODAL
        });
    }
}