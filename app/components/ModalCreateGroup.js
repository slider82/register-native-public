import React, { Component } from 'react';
import {
    ActivityIndicator,
    KeyboardAvoidingView,
    Modal,
    Text,
    TextInput,
    TouchableHighlight,
    ScrollView,
    StyleSheet,
    Switch,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import _ from 'lodash';

import Header from './Header';
import {
    createGroup,
    hideModal
} from '../actions/GroupActions';
import CheckBox from './CheckBox';

const DAYS = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
const DAYS_NAME = {
    Mo: 'Пн',
    Tu: 'Вт',
    We: 'Ср',
    Th: 'Чт',
    Fr: 'Пт',
    Sa: 'Сб'
};

const Validate = {
    name(name) {
        return name.trim();
    },

    students(students) {
        return students.length;
    },

    startEdu(start) {
        return /\d{1,2}\.\d{1,2}/.test(start);
    },

    endEdu(end) {
        return /\d{1,2}\.\d{1,2}/.test(end);
    },

    timeEdu(time) {
        return /\d{1,2}:\d{1,2}/.test(time);
    },

    daysEdu(days) {
        return days.some(day => DAYS.indexOf(day) !== -1);
    },

    creditsEdu(credits) {
        return typeof credits === 'number' && credits > 0 && credits <= 5;
    }
};

function getStudyPeriod() {
    let period,
        periods = {
            autumn: {
                start: '1.9',
                end: '31.12'
            },
            spring: {
                start: '1.2',
                end: '31.5'
            }
        },
        // now = new Date(2016, 0, 1),
        now = new Date(),
        nowMonth = now.getMonth() + 1;

    if (nowMonth < 6) {
        period = periods.spring;
    } else {
        period = periods.autumn;
    }

    return period;
}

class ModalCreateGroup extends Component {
    constructor(props) {
        super(props);

        const studyPeriod = getStudyPeriod();

        this.defaultValues = {
            name: '',
            students: '',
            lang: 'en',
            eduStart: studyPeriod.start,
            eduEnd: studyPeriod.end,
            eduTime: '9:00',
            eduCredits: '5',
            days: _.reduce(DAYS, (result, day) => _.extend(result, {
                [day]: false
            }), {})
        };

        this.errorsDefault = {
            name: false,
            students: false,

            startEdu: false,
            endEdu: false,
            timeEdu: false,
            daysEdu: false,
            creditsEdu: false
        };

        this.state = {
            ...this.defaultValues,

            error: false,
            errors: {
                ...this.errorsDefault
            }
        };

        this.onNameChange = this.onNameChange.bind(this);
        this.onStudentsChange = this.onStudentsChange.bind(this);
        this.onEduStartChange = this.onEduStartChange.bind(this);
        this.onEduEndChange = this.onEduEndChange.bind(this);
        this.onEduTimeChange = this.onEduTimeChange.bind(this);
        this.onDaysChange = this.onDaysChange.bind(this);
        this.onEduCreditsChange = this.onEduCreditsChange.bind(this);

        this.onPressSubmit = this.onPressSubmit.bind(this);
        this.onPressClose = this.onPressClose.bind(this);

        this.renderCheckBox = this.renderCheckBox.bind(this);
    }

    validate(data) {
        let isValid;
        let errors = {};
        let startEduMonth = +data.edu.start.split('.')[1];
        let endEduMonth = +data.edu.end.split('.')[1];

        Object.keys(data).forEach(key => {
            if (key === 'edu') {
                Object.keys(data[key]).forEach(edu => {
                    let eduKey = edu + 'Edu';
                    let validateFunc = Validate[eduKey];

                    if (validateFunc) {
                        if (!validateFunc(data[key][edu])) {
                            errors[eduKey] = true;
                        }
                    }
                });
            } else {
                let validateFunc = Validate[key];

                if (validateFunc) {
                    if (!validateFunc(data[key])) {
                        errors[key] = true;
                    }
                }
            }
        });

        // если начальный месяц больше следующего то ошибка
        if (startEduMonth > endEduMonth) {
            errors.startEdu = true;
            errors.endEdu = true;
        }

        isValid = !Object.keys(errors).length;

        return {
            isValid,
            errors
        };
    }

    getStudents() {
        return this.state.students
            .split('\n')
            .map(item => item.trim()) // if do valid remove it
            .filter(item => item.length) // if do valid remove it
            .map(
                item => item
                    .replace(/[^A-Za-zа-яА-я\s]/g, '')
                    .trim()
                    .replace(/\s+/g, ' ')
                    .split(' ')
                    .map(item => item && item[0].toUpperCase() + item.slice(1).toLowerCase())
                    .join(' ')
            )
            .filter(item => item.length)
            .sort();
    }

    getDays() {
        return _.reduce(this.state.days, (result, value, key) => {
            if (value) {
                result.push(key);
            }
            return result;
        }, []);
    }

    getResetErrors() {
        if (this.state.error) {
            return {
                error: false,
                errors: {
                    ...this.errorsDefault
                }
            };
        } else {
            return {};
        }
    }

    onChange(data) {
        let errors = {};
        if (this.state.error) {
            errors = {
                error: false,
                errors: {
                    ...this.errorsDefault
                }
            };
        }

        this.setState({
            ...data,
            ...errors
        });
    }

    onNameChange(value) {
        this.onChange({
            name: value.trim()
        });
    }

    onStudentsChange(value) {
        this.onChange({
            students: value
        });
    }

    onEduStartChange(value) {
        this.onChange({
            eduStart: value
        });
    }

    onEduEndChange(value) {
        this.onChange({
            eduEnd: value
        });
    }

    onEduTimeChange(value) {
        this.onChange({
            eduTime: value
        });
    }

    onEduCreditsChange(value) {
        this.onChange({
            eduCredits: value
        });
    }

    onDaysChange(day) {
        this.onChange({
            days: {
                ...this.state.days,
                ...day
            }
        });
    }

    onPressClose() {
        this.props.hideModal();
    }

    onPressSubmit() {
        let data = {
            teacher: this.props.teacher,
            type: 'main',
            name: this.state.name.trim(),
            lang: this.state.lang,
            students: this.getStudents(),
            edu: {
                start: this.state.eduStart.trim(),
                end: this.state.eduEnd.trim(),
                time: this.state.eduTime.trim(),
                days: this.getDays(),
                credits: +(this.state.eduCredits.trim()),
            }
        };
        let validate = this.validate(data);

        if (validate.isValid) {
            this.props.createGroup(data, this.props.token);
            this.props.hideModal();
        } else {
            this.setState({
                error: true,
                errors: {
                    ...this.state.errors,
                    ...validate.errors
                }
            });
        }
    }

    renderCheckBox(day, idx) {
        return <CheckBox
            style={styles.checkBox}
            onChange={this.onDaysChange}
            text={DAYS_NAME[day]}
            name={day}
            error={this.state.errors.daysEdu}
            key={idx}
        />;
    }

    render() {
        const isShowPreloader = false;
        const buttonDisabled = false;
        const buttonDisabledStyle = {};

        let errorStyles = {};

        if (this.state.error) {
            errorStyles = _.reduce(this.state.errors, (result, value, key) => {
                if (value) {
                    result = _.extend(result, {
                        [key]: styles.inputError
                    });
                }
                return result;
            }, {});
        }

        return (
            <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.props.showModal}
            >
                <View style={styles.container}>

                    <Header
                        textTitle="Создание группы"
                        onPressClose={this.onPressClose}
                    />

                    <KeyboardAvoidingView
                        behavior="padding"
                        style={styles.keyboard}
                        keyboardVerticalOffset={140}
                    >
                        <ScrollView style={styles.body}>
                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Название</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        onChangeText={this.onNameChange}
                                        style={[styles.input, errorStyles.name]}
                                        placeholder="Введите название группы"
                                        value={this.state.name}
                                    />
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Студенты</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        multiline={true}
                                        // android
                                        numberOfLines={3}
                                        onChangeText={this.onStudentsChange}
                                        style={[styles.input, styles.inputStudents, errorStyles.students]}
                                        placeholder="Введите список студентов, каждый с новой строки"
                                        value={this.state.students}
                                    />
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Начало занятий</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        onChangeText={this.onEduStartChange}
                                        style={[styles.input, errorStyles.startEdu]}
                                        placeholder="Введите дату начала занятий"
                                        value={this.state.eduStart}
                                    />
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Конец занятий</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        onChangeText={this.onEduEndChange}
                                        style={[styles.input, errorStyles.endEdu]}
                                        placeholder="Введите дату окончания занятий"
                                        value={this.state.eduEnd}
                                    />
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Время занятия</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        onChangeText={this.onEduTimeChange}
                                        style={[styles.input, errorStyles.timeEdu]}
                                        placeholder="Введите вермя начала занятия"
                                        value={this.state.eduTime}
                                    />
                                </View>
                            </View>

                            {/* icon checkbox http://www.flaticon.com/packs/font-awesome?word=check&group_id=25*/}

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Дни недели</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <View style={[styles.checkboxWrapper, styles.checkboxWrapperFirst]}>
                                        {
                                            DAYS.slice(0,3).map(this.renderCheckBox)
                                        }
                                    </View>
                                    <View style={styles.checkboxWrapper}>
                                        {
                                            DAYS.slice(3,6).map(this.renderCheckBox)
                                        }
                                    </View>
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Зачетных занятий</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        onChangeText={this.onEduCreditsChange}
                                        style={[styles.input, errorStyles.creditsEdu]}
                                        placeholder="Введите количество зачетных занятий"
                                        value={this.state.eduCredits}
                                    />
                                </View>
                            </View>


                            <View style={styles.row}>
                                <View style={styles.label}/>
                                <View style={styles.buttonWrapper}>
                                    <TouchableHighlight
                                        onPress={this.onPressSubmit}
                                        disabled={buttonDisabled}
                                        style={[styles.button, buttonDisabledStyle]}
                                    >
                                        <Text style={styles.buttonText}>Создать</Text>
                                    </TouchableHighlight>


                                    {isShowPreloader ? <ActivityIndicator /> : null}
                                </View>
                            </View>

                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </Modal>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },

    keyboard: {
        flex: 1
    },

    body: {
        flex: 1,
        padding: 17,
        // justifyContent: 'center',
        // paddingTop: 100
    },

    title: {
        flexDirection: 'row',
        marginBottom: 15,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#CCC',
    },

    student: {
        paddingBottom: 10
    },

    studentText: {
        fontSize: 18,
        fontWeight: '600'
    },

    date: {},
    dateText: {
        fontSize: 18,
        fontWeight: '600',
        // color: '#999'
    },

    row: {
        flexDirection: 'row',
        marginBottom: 15,
    },

    label: {
        flex: 2,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    labelText: {
        fontSize: 16
    },

    inputWrapper: {
        flex: 3,
    },
    checkboxWrapper: {
        flexDirection: 'row',
        height: 18
    },
    checkboxWrapperFirst: {
        marginBottom: 10
    },
    checkBox: {
        flex: 1,
    },
    switchWrapper: {
        alignItems: 'center'
    },
    input: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 4,
        fontSize: 16,
        height: 30,
        backgroundColor: '#FFF'
    },
    inputDisabled: {
        opacity: .5
    },
    inputStudents: {
        height: 80
    },
    inputError: {
        borderColor: '#a94442'
    },

    buttonWrapper: {
        flex: 3,
        flexDirection: 'row',
        paddingBottom: 17,
    },
    button: {
        height: 30,
        backgroundColor: '#337ab7',
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 4,
        marginRight: 10,
        padding: 10,
        justifyContent: 'center'
    },
    buttonDisabled: {
        opacity: .5
    },

    buttonText: {
        color: '#FFF',
    }

});

function mapStateToProps(state) {
    return {
        token: state.user.token,
        teacher: state.user._id,
        showModal: state.groups.showModal
    };
}

function mapDispatchToProps(dispatch) {
    return {
        createGroup: bindActionCreators(createGroup, dispatch),
        hideModal: bindActionCreators(hideModal, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalCreateGroup);