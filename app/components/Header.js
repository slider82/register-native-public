import React, { Component } from 'react';
import {
    Platform,
    Text,
    TouchableHighlight,
    StyleSheet,
    View,
} from 'react-native';

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const TITLE_OFFSET = Platform.OS === 'ios' ? 70 : 40;

class Header extends Component {
    constructor(props) {
        super(props);

        this.onPressClose = this.onPressClose.bind(this);
    }

    onPressClose() {
        this.props.onPressClose();
    }

    render() {
        return (
            <View style={styles.header}>
                <View style={styles.headerAppBar}>
                    <View style={[StyleSheet.absoluteFill, styles.headerRow]}>
                        <View style={[styles.headerItem, styles.headerTitle]}>
                            <Text style={styles.headerTitleText}>{this.props.textTitle}</Text>
                        </View>
                        <View style={[styles.headerItem, styles.headerRight]}>
                            <TouchableHighlight
                                style={styles.headerRightTouch}
                                activeOpacity={.7}
                                underlayColor="transparent"
                                onPress={() => this.onPressClose()}>
                                <Text style={styles.headerRightText}>Закрыть</Text>
                            </TouchableHighlight>
                        </View>

                    </View>
                </View>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    header: {
        paddingTop: STATUSBAR_HEIGHT,
        // backgroundColor: '#FFF',
        backgroundColor: Platform.OS === 'ios' ? '#EFEFF2' : '#FFF',
        height: STATUSBAR_HEIGHT + APPBAR_HEIGHT,
        borderBottomWidth: .5,
        borderBottomColor: 'rgba(0,0,0,.1)',
        shadowColor: 'black',
        // dosent work (((
        shadowOpacity: 0.1,
        shadowRadius: StyleSheet.hairlineWidth,
        shadowOffset: {
            height: StyleSheet.hairlineWidth,
        },
        // (((
        elevation: 4,
    },
    headerAppBar: {
        flex: 1
    },
    headerRow: {
        flexDirection: 'row',
    },
    headerItem: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    headerTitle: {
        bottom: 0,
        left: TITLE_OFFSET,
        right: TITLE_OFFSET,
        top: 0,
        position: 'absolute',
        alignItems: Platform.OS === 'ios' ? 'center' : 'flex-start',
    },
    headerTitleText: {
        fontSize: Platform.OS === 'ios' ? 17 : 18,
        fontWeight: Platform.OS === 'ios' ? '600' : '500',
        color: 'rgba(0, 0, 0, .9)',
        textAlign: Platform.OS === 'ios' ? 'center' : 'left',
        marginHorizontal: 16,
    },
    // modalLeft: {
    //     left: 0,
    //     bottom: 0,
    //     top: 0,
    //     position: 'absolute',
    // },
    headerRight: {
        right: 0,
        bottom: 0,
        top: 0,
        position: 'absolute',
    },
    headerRightTouch: {
        flex: 1,
        justifyContent: 'center'
    },
    headerRightText: {
        fontSize: 17,
        paddingLeft: 10,
        paddingRight: 10,
        color: Platform.select({
            ios: '#037aff',
        })
    },
});

export default Header;