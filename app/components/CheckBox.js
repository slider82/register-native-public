import React, { Component} from 'react';
import {
    Image,
    Text,
    TouchableWithoutFeedback,
    StyleSheet,
    View
} from 'react-native';

class CheckBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: !!this.props.checked
        };
        this.onPress = this.onPress.bind(this);
    }

    onPress() {
        const state = !this.state.checked;

        this.setState({
            checked: state
        });

        this.props.onChange({
            [this.props.name]: state
        });
    }

    renderCheckbox() {
        if (this.props.error) {
            return <Image style={styles.checkbox} source={require('../img/check-box-empty_error.png')} />;
        } else {
            if (this.state.checked) {
                return <Image style={styles.checkbox} source={require('../img/check.png')}/>;
            } else {
                return <Image style={styles.checkbox} source={require('../img/check-box-empty.png')}/>;
            }
        }
    }

    render() {
        const checkbox = this.renderCheckbox();
        const stylesError = this.props.error ? styles.textError : {};

        return (
            <View style={this.props.style}>
                <TouchableWithoutFeedback onPress={this.onPress}>
                    <View style={styles.container}>
                        { checkbox }
                        <Text style={[styles.text, stylesError]}>{this.props.text}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    checkbox: {
        width: 18,
        height: 18,
        marginRight: 8,
    },
    text: {
        fontSize: 16
    },
    textError: {
        color: '#a94442'
    }
});

export default CheckBox;