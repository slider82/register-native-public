import React, { Component } from 'react';
import {
    ActivityIndicator,
    KeyboardAvoidingView,
    Modal,
    Text,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    Switch,
    View,
} from 'react-native';
import _ from 'lodash';

import Header from './Header';

class ModalLesson extends Component {
    constructor(props) {
        super(props);

        this.state = {
            evaluation: '',
            presence: true,
            credit: false
        };

        this.onPresenceChange = this.onPresenceChange.bind(this);
        this.onEvaluationChange = this.onEvaluationChange.bind(this);
        this.onCreditChange = this.onCreditChange.bind(this);
        this.onPressSubmit = this.onPressSubmit.bind(this);
        this.onPressClose = this.onPressClose.bind(this);
    }

    componentWillReceiveProps(nextProps) {

        if (!this.props.modalVisible && nextProps.modalVisible) {
            this.setState({
                evaluation: _.isNumber(nextProps.lesson.evaluation) && nextProps.lesson.evaluation.toString() || '',
                presence: (nextProps.lesson.presence === false ? false : true),
                credit: !!nextProps.lesson.credit
            });
        }
    }

    onPresenceChange(value) {
        this.setState({
            presence: !!value
        });
    }

    onEvaluationChange(value) {
        if (!_.isNaN(+value)) {
            this.setState({
                evaluation: value
            });
        } else {
            this.setState({
                evaluation: ''
            });
        }

    }

    onCreditChange(value) {

        this.setState({
            credit: value
        });
    }

    onPressClose() {
        this.props.onPressCloseModal();
    }

    onPressSubmit() {
        let props = {
            credit: null,
            presence: null,
            evaluation: null
        };


        if (this.state.credit) {
            props.credit = true;
        } else {
            if (this.state.evaluation) {
                props.evaluation = +this.state.evaluation;
            } else {
                props.presence = this.state.presence;
            }
        }

        this.props.onPressSubmitModal(props);
    }
    render() {
        const isShowPreloader = false;
        const buttonDisabled = false;
        const buttonDisabledStyle = {};
        let evaluationDisabledStyle = {};

        let disableEvaluation = false,
            disablePresence = false,
            disableCredit = false;

        if (this.state.credit) {
            disableEvaluation = true;
            disablePresence = true;
            disableCredit = false;
        } else {
            if (this.state.evaluation) {
                disableEvaluation = false;
                disablePresence = true;
                disableCredit = true;
            } else {
                if (this.state.presence === false) {
                    disableEvaluation = true;
                    disablePresence = false;
                    disableCredit = true;
                } else {
                    disableEvaluation = false;
                    disablePresence = false;
                    disableCredit = false;
                }
            }
        }

        if (disableEvaluation) {
            evaluationDisabledStyle = styles.inputDisabled;
        }

        return (
            <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.props.modalVisible}
            >
                <View style={styles.container}>

                    <Header
                        textTitle="Ведомость студента"
                        onPressClose={this.onPressClose}
                    />

                    <KeyboardAvoidingView
                        behavior="padding"
                        style={styles.keyboard}
                    >
                        <View style={styles.body}>
                            <View style={styles.title}>
                                <View style={styles.student}>
                                    <Text style={styles.studentText}>{this.props.student.shortName}</Text>
                                </View>
                                <View style={styles.date}>
                                    <Text style={styles.dateText}>{this.props.date}</Text>
                                </View>
                            </View>


                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Оценка</Text>
                                </View>
                                <View style={styles.inputWrapper}>
                                    <TextInput
                                        autoCapitalize="none"
                                        //onFocus={this.onFocus}
                                        onChangeText={this.onEvaluationChange}
                                        style={[styles.input, evaluationDisabledStyle]}
                                        placeholder="Введите оценку"
                                        disabled={disableEvaluation}
                                        value={this.state.evaluation}
                                    />
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Присутствие</Text>
                                </View>
                                <View style={[styles.inputWrapper, styles.switchWrapper]}>
                                    <Switch
                                        onValueChange={this.onPresenceChange}
                                        value={this.state.presence}
                                        disabled={disablePresence}
                                    />
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.label}>
                                    <Text style={styles.labelText}>Зачет</Text>
                                </View>
                                <View style={[styles.inputWrapper, styles.switchWrapper]}>
                                    <Switch
                                        onValueChange={this.onCreditChange}
                                        value={this.state.credit}
                                        disabled={disableCredit}
                                    />
                                </View>
                            </View>

                            <View style={styles.row}>
                                <View style={styles.label}/>
                                <View style={styles.buttonWrapper}>
                                    <TouchableHighlight
                                        onPress={this.onPressSubmit}
                                        disabled={buttonDisabled}
                                        style={[styles.button, buttonDisabledStyle]}
                                    >
                                        <Text style={styles.buttonText}>Заполнить</Text>
                                    </TouchableHighlight>


                                    {isShowPreloader ? <ActivityIndicator /> : null}
                                </View>
                            </View>

                        </View>
                    </KeyboardAvoidingView>
                </View>
            </Modal>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    keyboard: {
        flex: 1
    },
    body: {
        flex: 1,
        padding: 17,
        justifyContent: 'center',
        // paddingTop: 100
    },

    title: {
        flexDirection: 'row',
        marginBottom: 15,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#CCC',
    },

    student: {
        paddingBottom: 10
    },

    studentText: {
        fontSize: 18,
        fontWeight: '600'
    },

    date: {
    },
    dateText: {
        fontSize: 18,
        fontWeight: '600',
    },

    row: {
        flexDirection: 'row',
        marginBottom: 15,
    },

    label: {
        flex: 2,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    labelText: {
        fontSize: 16
    },

    inputWrapper: {
        flex: 3,
    },
    switchWrapper: {
        alignItems: 'center'
    },
    input: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 4,
        fontSize: 16,
        height: 30,
        backgroundColor: '#FFF'
    },
    inputDisabled: {
        opacity: .5
    },
    // inputError: {
    //     borderColor: '#a94442'
    // },

    buttonWrapper: {
        flex: 3,
        flexDirection: 'row',
    },
    button: {
        height: 30,
        backgroundColor: '#337ab7',
        borderColor: '#337ab7',
        borderWidth: 1,
        borderRadius: 4,
        marginRight: 10,
        padding: 10,
        justifyContent: 'center'
    },
    buttonDisabled: {
        opacity: .5
    },

    buttonText: {
        color: '#FFF',
    }

});

export default ModalLesson;