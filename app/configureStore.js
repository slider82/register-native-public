'use strict';

import React from 'react';
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import _ from 'lodash';

import reducers from './redusers';

export default function configureStore(user) {
    const initialState = {};

    if (user) {
        initialState.user = JSON.parse(user);
    }

    const store = createStore(
        reducers,
        initialState,
        applyMiddleware(thunk)
    );

    let currentUser = store.getState().user;
    let unsubscribe = store.subscribe(() => {
        let previousUser = currentUser;

        currentUser = _.pick(store.getState().user, ['name', 'email', 'token', 'isAuthorized', '_id']);

        if (!previousUser.token && currentUser.token && previousUser.token !== currentUser.token) {
            AsyncStorage.setItem('user', JSON.stringify(currentUser));

        } else if (previousUser.token && !currentUser.token) {
            AsyncStorage.removeItem('user');
        }
    });

    return store;
}