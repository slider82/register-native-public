'use strict';

import _ from 'lodash';

export default function (url, options = {
    method: 'GET',
}, token) {
    let headers = {
        'Content-Type': 'application/json'
    };

    if (token) {
        headers.Authorization = 'bearer ' + token;
    }

    return fetch(url, _.assign({
        headers
    }, options)).then(res => {
        return res.json();
    });
}