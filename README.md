### Приложение позволяющее вести отчетность занятий преподавателям. ###

Мобильное приложение на **react-native** aналог веб приложения https://bitbucket.org/slider82/timetable-public использующее его API

### Для запуска локально вам потребуется ###

* зайти на https://expo.io и установить приложение
* склонировать https://bitbucket.org/slider82/timetable-public и запустить
* установить зависимости **npm install**
* затем запустить **npm start**
* скопировать ссылку или считать QR код из приложения expo

### Демо версия для expo ###

https://expo.io/@slider82/register-native

Для запуска только нужно установить приложение https://expo.io